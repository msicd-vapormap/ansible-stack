# Déploiement de la stack Docker Swarm de Vapormap sur un cluster Swarm avec Ansible

__Par__ : FETCHEPING FETCHEPING Rossif Borel  
__Email__ : rossifetcheping@outlook.fr  
__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet fil rouge  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-stack>  
__Pages Gitlab__: <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-stack>  
__Date__ : 27 Février 2023  

## Description

Ce projet permet de déployer et maintenir automatiquement la stack Docker Swarm de l'application Vapormap sur le cluster Swarm.  

* L'infrastruction du projet "terraform" du groupe "vapormap-cicd" doit être préalablement mise en place.  

* L'inventaire de l'infrastructure est généré par le projet "terraform"  

* Le cluster Swarm doit être préalablement configuré à l'aide du projet "ansible-swarm" du groupe "vapormap-cicd".  

* Les opérations sont effectuées à partir des playbooks Ansible.  

* Les tâches à exécuter sont organisées dans des rôles.  

* Les playbooks sont variabilisés pour permettre la personnalisation de l'environnement de production.  

* L'un des fichiers de variables Ansible est généré à partir du template __group_vars/all/env.yml.template__ et des variables d'environnement.  

* La connexion SSH aux instances est configurée à l'aide du fichier __ssh.conf.template__ et des variables d'environnement. Le fichier __ssh.conf__ va être généré et utilisé par la configuration d'Ansible.  

* La stack Docker Swarm est disponible dans le projet "docker" et téléchargée lors du déploiement via Ansible grâce à un token d'accès à la registry.  

* Les images Docker de la stack sont dans la registry du projet "docker" et accessibles avec un token d'accès au dépôt.  

* Le fichier __.env__ des variables de la stack est templaté en Jinja et contenu dans le rôle __swarm_stack__. Il est configurable grâce aux variables Ansible du projet.  

* Docker Compose est utilisé pour configurer la stack avec le fichier .env sur les noeuds avant le déploiement.  

## Utilisation du dépôt

Pour déployer l'application sur cluster Swarm, deux modes opératoires sont disponibles :  

* [Déploiement depuis un poste local](./local.md)  
* [Déploiement depuis un pipeline Gitlab CD](./pipeline.md)  

## Présentation de l'infrastructure Cloud

* L'infrastructure est constituée de trois noeuds contenus dans un réseau privé accessible en SSH à partir d'une instance bastion.  
* Le noeud manager est rattaché à une IP flottante et expose des ports HTTP sur internet pour rendre l'application accessible.  
* L'instance bastion est rattachée à une IP flottante et expose le port SSH sur internet pour permettre le déploiement de la stack.  
