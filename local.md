# Déploiement de la stack Docker Swarm de Vapormap sur un cluster Swarm avec Ansible à partir d'un poste local

__Par__: FETCHEPING FETCHEPING Rossif Borel  
__Email__: rossifetcheping@outlook.fr  
__École__: IMT Atlantique  
__Formation__: Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__: 2022-2023  
__UE__: Projet fil rouge  
__Dépôt Gitlab__: <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-stack>  
__Pages Gitlab__: <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-stack>  
__Date__: 27 Février 2023  

## Prérequis

* La configuration du cluster avec le projet "ansible-swarm"
* La disponibilité des images Docker de la stack Swarm dans la registry du projet "docker"  
* Le token d'accès à la registry privée des images du groupe vapormap-cicd
* Le token d'accès au dépot du projet "docker"  
* Le déploiement de l'infrastructure du projet "terraform"  
* Le fichier __public_ip.ini__ du répertoire __inventory/__ du projet "terraform" contenant les IP flottantes des instances  
* Le fichier __hosts.ini__ du répertoire __inventory/__ du projet "terraform" contenant l'inventaire des noeuds de l'infrastructure  
* La clé privée SSH de connexion aux instances  

## Préparer le système

* Mettre à jour le système

```sh
sudo apt update
```

* Installer les pré-requis : Environnement Python

```sh
sudo apt -y install python3 python3-pip python3-venv
```

## Configurer l'environnement de l'application

* Récupérer le dépôt  

```sh
cd $HOME
git clone git@gitlab.imt-atlantique.fr:vapormap-cicd/ansible-stack.git vapormap/ansible-stack
```

* Créer et activer l'environnement virtuel Python

```sh
cd $HOME/vapormap/ansible-stack
python3 venv venv/ansible-stack
source venv/ansible-stack/bin/activate
```

* Installer les dépendances Python

```sh
pip install -r requirements/python-venv.txt
```

* Installer des collections Ansible

```sh
ansible-galaxy collection install -r requirements/ansible-collections.yml
```

* Copier l'inventaire généré par Terraform  

```bash
cd $HOME/vapormap/ansible-stack
cp ../terraform/inventory/hosts.ini hosts.ini
```

* Générer le fichier des variables Ansible __group_vars/all/env.yml__  

```sh
cd $HOME/vapormap/ansible-stack

export NODE_PUBLIC_IP='PUBLIC_IP_MANAGER' # Valeur à remplacer

export CI_REGISTRY='gitlab-registry.imt-atlantique.fr'
export CI_REGISTRY_USER='gitlab-ci-token'
export TOKEN_REGISTRY_VAP='glpat-1PRkWBacpmxzM9F9xVMc'

export TAG_IMAGE_DB=10.9
export TAG_IMAGE_API=1.1
export TAG_IMAGE_FRONT=1.0

export DB_NAME=db_vapormap
export DB_USER=user_vapormap
export DB_PASS=vapormap

export CI_API_V4_URL='https://gitlab.imt-atlantique.fr/api/v4'
export DOCKER_PROJECT_ID=2031
export TOKEN_DOCKER_REPO='glpat-zj7KggiqiYyWKspUKmUs'

envsubst '$NODE_PUBLIC_IP, $CI_REGISTRY, $CI_REGISTRY_USER, $TOKEN_REGISTRY_VAP, $TAG_IMAGE_DB, $TAG_IMAGE_API, $TAG_IMAGE_FRONT, $DB_NAME, $DB_USER, $DB_PASS,  $CI_API_V4_URL, $DOCKER_PROJECT_ID, $TOKEN_DOCKER_REPO' < group_vars/all/env.yml.template > group_vars/all/env.yml
```

> Remarque : Remplacer __PUBLIC_IP_MANAGER__ par l'adresse IP publique du noeud manager. Elle est disponible dans le fichier __public_ip.ini__ du repertoire __inventory/__ du projet "terraform"  

* Configurer la connexion SSH aux noeuds: générer le fichier ssh.conf  

```sh
cd $HOME/vapormap/ansible-stack

export BASTION_PUBLIC_IP='PUBLIC_IP_BASTION' # Valeur à remplacer
export BASTION_USER='ubuntu'
export NODE_USER='ubuntu'
export PRIVATE_KEY_PATH='keys/private_key.pem' # Valeur à remplacer

envsubst '$BASTION_PUBLIC_IP, $BASTION_USER, $NODE_USER, $PRIVATE_KEY_PATH' < ssh.conf.template > ssh.conf
```

> Remarque: Le répertoire __keys/__ est prévu pour garantir la sécurité de la clé si jamais vous souhaitez l'avoir à proximité dans le projet. Tous les fichiers de ce dossier sont systématiquement ignorés lors des commits.  

## Déployer Vapormap sur le cluster

* Lancer le Playbook de déploiement  

```sh
cd $HOME/vapormap/ansible-stack
source venv/ansible-stack/bin/activate

ansible-playbook deploy.yml
```

## Tester l'application

Depuis un navigateur : <http://PUBLIC_IP_MANAGER:8000>

## Supprimer l'installation sur les noeuds

```sh
ansible-playbook destroy.yml
```

## Annexe : tests de qualité de code

* Évaluer la qualité de code du projet avec Ansible-lint et Yamllint  

```sh
cd $HOME/vapormap/ansible-stack
source venv/ansible-stack/bin/activate

make lint
```
